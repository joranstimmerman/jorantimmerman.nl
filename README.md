# jorantimmerman.nl

The source code of jorantimmerman.nl

## Built With

* [ASP.NET Core ](https://docs.microsoft.com/nl-nl/aspnet/core/?view=aspnetcore-5.0) - Web Api Framework
* [React](https://reactjs.org/) - Javascript Framework
* [Ant Design](https://ant.design/) - React UI library

## Authors

* **Joran Timmerman** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details
