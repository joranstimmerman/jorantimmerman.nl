using CasCap.Models;
using CasCap.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Http;
using backend.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace backend.Repository
{
    public class PhotoRepository
    {
        private readonly DatabaseContext _context;

        public PhotoRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task<PhotoClassification> addPhoto(PhotoClassification contact)
        {
            _context.Add(contact);
            await _context.SaveChangesAsync();
            return contact;
        }

        public async Task<PhotoClassification> getPhotoById(int id)
        {
            return await _context.PhotoClassifications.FirstOrDefaultAsync(p => p.PhotoId == id);
        }

    }

    public class PhotosClient
    {
        public static GooglePhotosService newPhotosCient()
        {
            //new-up logging
            var logger = new LoggerFactory().CreateLogger<GooglePhotosService>();

            //new-up configuration options
            var options = new GooglePhotosOptions
            {
                User = System.Environment.GetEnvironmentVariable("GOOGLE_ACCOUNT"),
                ClientId = System.Environment.GetEnvironmentVariable("GOOGLE_PHOTOS_API_CLIENT_ID"),
                ClientSecret = System.Environment.GetEnvironmentVariable("GOOGLE_PHOTOS_API_CLIENT_SECRET"),
                Scopes = new[] { GooglePhotosScope.ReadOnly },
            };

            //new-up a single HttpClient
            var handler = new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate };
            var client = new HttpClient(handler) { BaseAddress = new Uri(options.BaseAddress) };

            //new-up the GooglePhotosService and pass in the logger, options and HttpClient
            var googlePhotosSvc = new GooglePhotosService(logger, Options.Create(options), client);
            return googlePhotosSvc;

            // //attempt to log-in
            // if (!await googlePhotosSvc.LoginAsync())
            //     throw new Exception($"login failed!");

            // //get and list all albums
            // var albums = await googlePhotosSvc.GetAlbums();
            // foreach (var album in albums)
            // {
            //     Console.WriteLine(album.title);
            // }
        }
    }
}
