using CasCap.Models;
using CasCap.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Http;
using backend.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace backend.Repository
{
    public interface IContactRepository
    {
        Task<Contact> addContact(Contact contact);
        Task<Contact> getContactById(int id);
        Task<IEnumerable<Contact>> getAllContacts();
        Task<Contact> deleteContact(Contact contact);
    }

    public class ContactRepository : IContactRepository
    {
        private readonly DatabaseContext _context;

        public ContactRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task<Contact> addContact(Contact contact)
        {
            _context.Add(contact);
            await _context.SaveChangesAsync();
            return contact;
        }

        public async Task<Contact> getContactById(int id)
        {
            return await _context.Contacts.FindAsync(id);
        }

        public async Task<IEnumerable<Contact>> getAllContacts()
        {
            var test = await _context.Contacts.ToListAsync();
            return test;
        }

        public async Task<Contact> deleteContact(Contact contact)
        {
            _context.Contacts.Remove(contact);
            await _context.SaveChangesAsync();
            return contact;
        }
    }
}
