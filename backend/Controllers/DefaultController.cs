using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CasCap.Services;
using CasCap.Models;
using backend;
using backend.Models;

namespace backend.Controllers
{
    [ApiController]
    [Route("api/")]
    public class DefaultController : ControllerBase
    {
        private readonly ILogger<DefaultController> _logger;

        public DefaultController(ILogger<DefaultController> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public object Get()
        {
            var responseObject = new
            {
                Status = "GET Ok"
            };

            _logger.LogInformation($"Status pinged: {responseObject.Status}");
            return responseObject;
        }
    }
}