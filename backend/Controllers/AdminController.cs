using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using backend.Models;

namespace backend.Controllers
{
    [ApiController]
    [Route("api/admin")]
    public class AdminController : ControllerBase
    {
        private readonly ILogger<DefaultController> _logger;

        public AdminController(ILogger<DefaultController> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        [Route("upload")]
        public async Task<IActionResult> UploadImage([FromForm]FileFormModel file)
        {
            






            // try
            // {
            //     string path = Path.Combine(Directory.GetCurrentDirectory(), file.FileName);

            //     using (Stream stream = new FileStream(path, FileMode.Create))
            //     {
            //         file.File.CopyTo(stream);
            //     }

            //     return Ok();
            // }
            // catch {
            //     return BadRequest();
            // }

            // Console.WriteLine(Request.Form.Files[0]);

            // if (files.Count < 1)
            // {
            //     return BadRequest();
            // }

            // Console.WriteLine(files);

            return Ok();
        }
    }
}