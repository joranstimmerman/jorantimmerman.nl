using Microsoft.AspNetCore.Http;

namespace backend.Models
{
    public class FileFormModel
    {

        public string FileName { get; set; }
        public IFormFile FileForm { get; set; }
    }
}