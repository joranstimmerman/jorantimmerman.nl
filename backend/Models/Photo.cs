using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;

namespace backend.Models
{
    public class PhotoClassification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PhotoId { get; set; }

        public string Path { get; set; }

        public string Location { get; set; }

        public string Date { get; set; }
    }

    // public class PhotoDetails {
    //     [Required]
    //     public Image image { get; set; }

    //     [Required]
    //     public string location { get; set; }

    //     [Required]
    //     public string date { get; set; }
    // }
}