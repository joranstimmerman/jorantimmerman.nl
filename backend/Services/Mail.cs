using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace backend.Services
{
    public class MailClient
    {
        public static SmtpClient NewSmtpClient() {
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com")
            {
                UseDefaultCredentials = false,
                Port = 587,
                Credentials = new NetworkCredential(
                  System.Environment.GetEnvironmentVariable("GOOGLE_ACCOUNT"),
                  System.Environment.GetEnvironmentVariable("GOOGLE_ACCOUNT_PASSWORD")),
                EnableSsl = true,
            };

            return smtpClient;
        }
    }
}