import React from "react";
import { Col } from "antd";
import { Link } from "react-router-dom";
import { HomeOutlined, PictureOutlined, MailOutlined } from "@ant-design/icons";

import "../../assets/admin.sass";

class AdminSider extends React.Component {
  constructor(props) {
    super(props);
    this.state = { width: 0, height: 0 };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  render() {
    if (this.state.width > 1150) {
      return (
        <div className="sider">
          <Col span={24} className="sider_item">
            <Link className="sider_link" to="/admin">
              <HomeOutlined className="icon" />
              Admin Overview
            </Link>
          </Col>
          <Col span={24} className="sider_item">
            <Link className="sider_link" to="/admin/upload">
              <PictureOutlined className="icon" />
              Upload Image
            </Link>
          </Col>
          <Col span={24} className="sider_item">
            <Link className="sider_link" to="/admin/contact">
              <MailOutlined className="icon" />
              Contact Requests
            </Link>
          </Col>
        </div>
      );
    } else {
      return (
        <div className="sider">
          <Col span={24} className="sider_item">
            <Link className="sider_link" to="/admin">
              <HomeOutlined className="icon" />
            </Link>
          </Col>
          <Col span={24} className="sider_item">
            <Link className="sider_link" to="/admin/upload">
              <PictureOutlined className="icon" />
            </Link>
          </Col>
          <Col span={24} className="sider_item">
            <Link className="sider_link" to="/admin/contact">
              <MailOutlined className="icon" />
            </Link>
          </Col>
        </div>
      );
    }
  }
}

export default AdminSider;
