import React from "react";
import HomepageHeader from "./HomepageHeader";
import HomepageMenu from "./HomepageMenu";
import Footer from "./footer";
import background from "../images/IMG_1634.jpg";

import "../assets/homepage.sass";
// import API from "../Api.js"

class Homepage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { width: 0, height: 0 };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  render() {
    if (this.state.width > 1050) {
      return (
        <div
          className="container"
          style={{ backgroundImage: `url(${background})` }}
        >
          <HomepageHeader />
          <Footer />
        </div>
      );
    } else {
      return (
        <div
          className="container"
          style={{ backgroundImage: `url(${background})` }}
        >
          <HomepageMenu />
          <Footer />
        </div>
      );
    }
  }
}

export default Homepage;
