import React from "react";
import { Row, Col } from "antd";
import { Link, NavLink } from "react-router-dom";

import logo from "../images/logo_white.png";
import {
  HomeOutlined,
  PictureOutlined,
  UserOutlined,
  MailOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from "@ant-design/icons";

import "../assets/header.sass";

class Head extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: window.location.href.split("/")[3],
      width: 0,
      height: 0,
      collapsed: false,
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  toggleCollapsed() {
    this.setState({ collapsed: !this.state.collapsed });
  }

  render() {
    if (this.state.width > 1150) {
      return (
        <div className="nav jumbotron">
          <Row className="navigation">
            <Col span={9}>
              <div className="item">
                <Link className="link" to="/">
                  <HomeOutlined className="icon" />
                  Home
                </Link>
              </div>
              <div className="item">
                <Link
                  className={`link ${
                    this.state.location === "portfolio" ? "selected" : ""
                  }`}
                  to="/portfolio"
                >
                  <PictureOutlined className="icon" />
                  Portfolio
                </Link>
              </div>
            </Col>
            <Col span={6}>
              <div className="header_logo_container">
                <img className="header_logo" alt="logo" src={logo} />
              </div>
            </Col>
            <Col span={9}>
              <div className="item">
                <Link
                  className={`link ${
                    this.state.location === "about" ? "selected" : ""
                  }`}
                  to="/about"
                >
                  <UserOutlined className="icon" />
                  About
                </Link>
              </div>
              <div className="item">
                <Link
                  className={`link ${
                    this.state.location === "contact" ? "selected" : ""
                  }`}
                  to="/contact"
                >
                  <MailOutlined className="icon" />
                  Contact
                </Link>
              </div>
            </Col>
          </Row>
        </div>
      );
    } else {
      if (!this.state.collapsed) {
        return (
          <div>
            <button
              className="menubutton"
              onClick={() => this.toggleCollapsed()}
            >
              <MenuUnfoldOutlined style={{ color: "#b3b3b3" }} />
            </button>
            <div className="logo">
              <img alt="logo" src={logo} />
            </div>
          </div>
        );
      } else {
        return (
          <div>
            <button
              className="menubutton"
              onClick={() => this.toggleCollapsed()}
            >
              <MenuFoldOutlined style={{ color: "#b3b3b3" }} />
            </button>
            <div className="dropdown">
              <Row>
                <Col span={24}>
                  <NavLink className="dropdownitem" to="/portfolio">
                    Portfolio
                  </NavLink>
                </Col>
                <Col span={24}>
                  <NavLink className="dropdownitem" to="/about">
                    About
                  </NavLink>
                </Col>
                <Col span={24}>
                  <a
                    className="dropdownitem"
                    href="https://www.instagram.com/jorantimmermanphotography/"
                  >
                    Instagram
                  </a>
                </Col>
                <Col span={24}>
                  <NavLink className="dropdownitem" to="/contact">
                    Contact
                  </NavLink>
                </Col>
              </Row>
            </div>
            <div className="logo">
              <img alt="logo" src={logo} />
            </div>
          </div>
        );
      }
    }
  }
}

export default Head;
